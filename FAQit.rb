#!/usr/bin/env ruby

require "rubygems"

require "awesome_print"
require "yaml"
require "nokogiri"
require "open-uri"
require "progressbar"
require "benchmark"
require "uri"

require_relative "lib/SRToolsUtilities"

SCRIPT = <<EOS
  $(document).ready(function(){
    $("dd.hidden").slideUp();

    $("dt").click(function(){
      var $this = $(this);
      var $sib = $this.siblings('dd');

      if ($sib.hasClass("active")) {
        $sib.slideUp().removeClass('active')
      }
      else
      {
        $('.active').slideUp().removeClass('active');
        $sib.slideDown().addClass('active');
      }
    });
  });
EOS

def emit_qa(q,a)
  "<dt>#{q}</dt><dd style=\"display: none;\">#{a}</dd>"
end

def push_head
    head = $doc.css "head"
    script = Nokogiri::XML::Node.new "script", $doc
    script["src"] = "http://code.jquery.com/jquery-2.0.3.min.js"
    script["type"] = "text/javascript"
    script.parent = head.first
    # <script src="http://code.jquery.com/jquery-2.0.3.min.js" type="></script>
    # <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    # <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
end

def push_script
    body = $doc.css "body"
    js1 = Nokogiri::XML::Node.new "script", $doc
    js1["src"] = "//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"
    js1.parent = body.first
    js2 = Nokogiri::XML::Node.new "script", $doc
    js2.content = SCRIPT
    js2.parent = body.first
end

def html_format_to_new
  # h3 is the question, parent is a div, and child is a p with answer
  qs = $doc.css "h3"
  qs.each do |q|
    q.name = "dt"
    q.parent.name = "dl"
    ans = q.next_element
    ans.name = "dd"
    ans["style"] = "display: none;"
  end
end

def url_phrase(phrase)
  result = phrase.downcase.gsub(/[!,\?\.]/, "").gsub(/[ &\/\\]/, "_")
  return URI.escape(result)
end

# <h2>Basic Actions</h2>
# <a name="folders">
def anchor_headers
  headers = $doc.css "h2"
  pbar = ProgressBar.new "Headers", headers.count
  headers.each do |h|
    if h.css("a").empty?
      anchor = Nokogiri::XML::Node.new "a", $doc
      anchor["name"] = url_phrase(h.text)
      h.add_child(anchor)
    end
    pbar.inc
  end
  pbar.finish
end

# <dt>What are the menu options?</dt>
# <a id="..." href="#...">
def anchor_questions
  qs = $doc.css "dt"
  pbar = ProgressBar.new "Questions", qs.count
  qs.each do |q|
    if q.css("a").empty?
      anchor = Nokogiri::XML::Node.new "a", $doc
      phrase = url_phrase(q.text)
      anchor["id"] = phrase
      anchor["href"] = "\##{phrase}"
      q.add_child(anchor)
    end
    pbar.inc
  end
  pbar.finish
end

def main
  time = Benchmark.realtime do
    SRToolsUtilities::Configurator.instance.path = "/../config/FAQit.yml"
    config = SRToolsUtilities::Configurator.instance.load

    $doc = Nokogiri::HTML::parse(open(config["faq_in"]))
    anchor_headers
    anchor_questions

    File.open(config["faq_out"], 'w') { |file| file.write($doc.to_html) }
  end
  puts "Time elapsed #{time} seconds"
end

# As executable - main
if __FILE__ == $PROGRAM_NAME
  main
end
