#!/usr/bin/env ruby

require "rubygems"

require "yaml"
require "singleton"
require "active_support/all"
require "mysql2"
require "logger"
require "psych"
require "redis"
require "awesome_print"

module AwesomePrint
  module Logger

    def ap(object, level = nil)
      level ||= AwesomePrint.defaults[:log_level] if AwesomePrint.defaults
      level ||= :debug
      send level, object.ai
    end
  end
end

module SRToolsUtilities

  class Configurator
    include Singleton

    attr_reader :config, :redis
    attr_writer :path, :env

    def initialize
      @path = "/../config/config.yml"
      @env = nil
    end

    def load
      begin
        @config = YAML.load_file(__dir__ + @path)
        @config = @config[@env] if @env
        @redis = Redis.new(@config['cache']) if @config['cache']
        @config
      rescue Exception => e
        puts e.message
      end
    end

  end # Configurator

  class DAL
    include Singleton

    def initialize
      begin
        Configurator.instance.load if Configurator.instance.config.nil?
        settings = Configurator.instance.config['db'].merge Configurator.instance.config['schema']
        ActiveRecord::Base.establish_connection(settings);
        if Configurator.instance.config['db']['debug']
          ActiveRecord::Base.logger = Logger.new(STDERR)
          ActiveSupport::LogSubscriber.colorize_logging = false # enable for terminal
        end
      rescue Exception => e
        puts e.message
      end
    end

  end # DAL

  class OAuthServices
    include Singleton

    attr_reader :enabled

    def initialize
      python = `which python`
      @enabled = !python.nil? and python.length > 1 and File.exists?("oauth2.py")
    end

    # Stake first line, element after ":" without spaces for token, second line for expiry
    def get_access_token(account)
      begin
        exec_string = "python oauth2.py --client_id=#{$config.config['application']['client_id']} --client_secret=#{$config.config['application']['client_secret']} --refresh_token=#{account['m_refresh_token']}"
        result = `#{exec_string}`
        [result.split("\n")[0].split(":")[1].strip, result.split("\n")[1].split(":")[1].strip]
      rescue Exception => e
        puts e.message
      end if @enabled
    end

    # Execute IMAP - end is (Success) or (Failure) with $&  - better is using shell error
    def test_access_token(account, access_token)
      begin
        exec_string = "python oauth2.py --test_imap_authentication --user=#{account['addr']} --access_token=#{access_token}"
        `#{exec_string}`.match(/\((\w*)\)$/)
        $?.exitstatus # $CHILD_STATUS
      rescue Exception => e
        puts e.message
      end if @enabled
    end

    # TODO: interactive
    def make_refresh_token(account)
      begin
        # exec_string = "python oauth2.py --user=popthevolume@gmail.com --generate_oauth2_token --client_id= --client_secret="
        exec_string = "python oauth2.py --user=#{account['addr']} --generate_oauth2_token --client_id=#{$config.config['application']['client_id']} --client_secret=#{$config.config['application']['client_secret']}"
      rescue Exception => e
        puts e.message
      end if @enabled
    end

  end # OAuthServices

  class Utils
    include Singleton

    def domain_of(addr)
      (addr.match(/@(.*)/) && $1) || addr
    end

    def is_domain?(addr)
        (addr =~ /@/).nil?
    end

  end # Utils

end # SRToolsUtilities

# Local Debug
def main
  SRToolsUtilities::Configurator.instance.path = "/../config/keywords.yml"
  config = SRToolsUtilities::Configurator.instance.load
  puts config
 puts ">>> Done"
end

# As executable - main
if __FILE__ == $PROGRAM_NAME
  main
end
